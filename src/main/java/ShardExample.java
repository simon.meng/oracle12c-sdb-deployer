import oracle.jdbc.OracleShardingKey;
import oracle.jdbc.OracleType;
import oracle.jdbc.pool.OracleDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ShardExample {
    public static void main(String[] args) throws SQLException {
        String url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(protocol=tcp)(HOST=gsm1)(port=1522))(connect_data=(service_name=oltp_rw_srvc.shdb.oradbcloud)(region=dc1)))";
        String user = "apptest", pwd = "oracle";
        OracleDataSource ods = new OracleDataSource();
        ods.setURL(url);
        ods.setUser(user);
        ods.setPassword(pwd);
        OracleShardingKey shardingKey =
                ods.createShardingKeyBuilder()
                        .subkey("james.parker@x.bogus4", OracleType.VARCHAR2)
                        .build();
        Connection conn = ods.createConnectionBuilder()
                .shardingKey(shardingKey)
                .build();
        Statement stmt = conn.createStatement();
        ResultSet resultSet =  stmt.executeQuery("select * from ACTIVITY");
        while (resultSet.next()) {
            System.out.println(resultSet.getString(1));
        }
        stmt.close();
        conn.close();
        System.out.println("2222");
    }
}