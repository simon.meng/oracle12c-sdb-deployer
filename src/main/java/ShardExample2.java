import oracle.jdbc.OracleConnection;
import oracle.ucp.admin.UniversalConnectionPoolManager;
import oracle.ucp.admin.UniversalConnectionPoolManagerImpl;
import oracle.jdbc.OracleShardingKey;
import oracle.jdbc.OracleType;
import oracle.jdbc.pool.OracleConnectionPoolDataSource;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import org.apache.commons.lang3.RandomStringUtils;
import oracle.jdbc.driver.T4CConnection;
import oracle.ucp.UniversalConnectionPoolAdapter;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ShardExample2 {
    private static LinkedBlockingQueue<List<Object>> sh1 = new LinkedBlockingQueue(20000);
    private static LinkedBlockingQueue<List<Object>> sh2 = new LinkedBlockingQueue(20000);
    private static int count = 10000000;
    public static void main(String[] args) throws Exception {
        final UniversalConnectionPoolManager mgr = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
        String url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(protocol=tcp)(HOST=gsm1)(port=1522))(connect_data=(service_name=oltp_rw_srvc.shdb.oradbcloud)(region=dc1)))";
        String user = "apptest", pwd = "oracle";
        PoolDataSource poolDataSource = PoolDataSourceFactory.getPoolDataSource();
        poolDataSource.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
        poolDataSource.setURL(url);
        poolDataSource.setUser(user);
        poolDataSource.setPassword(pwd);
        poolDataSource.setInitialPoolSize(20);
        poolDataSource.setInactiveConnectionTimeout(3);
        poolDataSource.setMaxPoolSize(20);
        //poolDataSource.setConnectionPoolName(poolName);
        //mgr.createConnectionPool((UniversalConnectionPoolAdapter) poolDataSource);

        for (int i = 0; i < 1; i++) {
            new Thread(() -> {
                for (int c = 0; c < 1000000; c++) {
                    try {
                        //String target = RandomStringUtils.randomAlphanumeric(3) + RandomUtils.nextInt(100000, 300000) + "12345646546";

//                        OracleShardingKey sk = poolDataSource.createShardingKeyBuilder()
//                                .subkey(target, OracleType.VARCHAR2)
//                                .build();
//
//                        OracleConnection conn =
//                                (OracleConnection) poolDataSource.createConnectionBuilder()
//                                        .shardingKey(sk)
//                                        .build();


                        OracleShardingKey shardingKey =
                                poolDataSource.createShardingKeyBuilder()
                                        .subkey("james.parker@x.bogus4", OracleType.VARCHAR2)
                                        .build();

                        Connection conn = poolDataSource.createConnectionBuilder()
                                .shardingKey(shardingKey)
                                .build();

                        T4CConnection nn = (T4CConnection) conn.getMetaData().getConnection();
                        List list = new ArrayList(20);
                        //list.add(target);
                        list.add(getAndIncrement());
                        list.add(RandomStringUtils.randomAlphanumeric(5) + "abcde");
                        list.add("4321431" + RandomStringUtils.randomAlphanumeric(5));
                        list.add("SDFSFDs" + RandomStringUtils.randomAlphanumeric(5));
                        for (int t = 5; t < 20; t++) {
                            list.add(RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(10, 20)));
                        }
                        if ("sh1".equals(nn.getServerSessionInfo().getProperty("AUTH_INSTANCENAME"))) {
                            sh1.put(list);
                        } else {
                            //sh2.put(list);
                        }
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }



        for (int i = 0; i < 4; i++) {
            new Thread(() -> {
                int cnt = 0;
                long start = System.currentTimeMillis();
                long begin = System.currentTimeMillis();
                try {
                    OracleShardingKey sk = poolDataSource.createShardingKeyBuilder().subkey("ABCDEFGHIJKLK270470", OracleType.VARCHAR2).build();
                    OracleConnection connnection = (OracleConnection) poolDataSource.createConnectionBuilder().shardingKey(sk).build();
                    connnection.setAutoCommit(false);
                    String sql = "INSERT INTO T VALUES" + "(" + get("?", ",", 20) + ")";
                    PreparedStatement ps = connnection.prepareStatement(sql);
                    while (true) {
                        List list = sh1.take();
                        for (int j = 0; j < list.size(); j++) {
                            if(j ==1) {
                                ps.setInt(j + 1, Integer.parseInt(list.get(1).toString()));
                            }else  {
                                ps.setString(j + 1, list.get(j).toString());
                            }
                        }
                        ps.addBatch();
                        if (++cnt % 1000 == 0) {
                            ps.executeBatch();
                            //ps.close();
                            connnection.commit();
                            long current = System.currentTimeMillis();
                            System.out.println(Thread.currentThread().getName() + " "+ cnt + ", " + " spent" + ((current - begin)/ 1000)+  "sec, 1000 record insert spent mis" + (current - start));
                            start = current;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            ).start();
        }


        Executors.newSingleThreadScheduledExecutor().schedule(()->{
            System.out.println(sh1.size() + " size");
        }, 3, TimeUnit.SECONDS);
    }

    public synchronized static int getAndIncrement() {
        return ++count;
    }

    public static String get(String str, String spe, int count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; i++) {
            sb.append(str);
            if (i != count - 1) {
                sb.append(spe);
            }
        }
        return sb.toString();
    }
}
