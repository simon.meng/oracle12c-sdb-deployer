import oracle.jdbc.OracleShardingKey;
import oracle.jdbc.OracleType;
import oracle.jdbc.pool.OracleDataSource;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ShardExample3 {
    public static void main(String[] args) throws SQLException {
        String url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(protocol=tcp)(HOST=gsm1)(port=1522))(connect_data=(service_name=oltp_rw_srvc.shdb.oradbcloud)(region=dc1)))";
        String user = "apptest", pwd = "oracle";
        PoolDataSource poolDataSource = PoolDataSourceFactory.getPoolDataSource();
        poolDataSource.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
        poolDataSource.setURL(url);
        poolDataSource.setUser(user);
        poolDataSource.setPassword(pwd);
        poolDataSource.setInitialPoolSize(10);
        poolDataSource.setMaxPoolSize(10);


        OracleShardingKey shardingKey =
                poolDataSource.createShardingKeyBuilder()
                        .subkey("james.parker@x.bogus", OracleType.VARCHAR2)
                        .build();

        Connection conn = poolDataSource.createConnectionBuilder()
                .shardingKey(shardingKey)
                .build();
        conn.close();
        System.out.println(conn);
    }

}