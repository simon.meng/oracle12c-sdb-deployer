
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * Created by root on 1/29/18.
 */
public class T {

    public static void main(String[] args) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update("$1".getBytes(StandardCharsets.UTF_8));
        byte[] bytes = md.digest("oracle".getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }
        System.out.println(sb);
    }
}
