#!/bin/sh
export ORACLE_SID={{sid}}
export ORACLE_HOME={{oracle_home}}
export PATH={{oracle_home}}/bin:/usr/sbin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin

sqlplus / as sysdba << EOF

spool /home/oracle/modify_db_files.lst
alter system set db_files = 2048 scope = spfile;
alter system set db_recovery_file_dest_size=20g scope=both;

shutdown immediate;
startup;
spool off;
EOF
