#!/bin/sh

export ORACLE_SID={{sid}}
export ORACLE_HOME={{oracle_home}}
export PATH={{oracle_home}}/bin:/usr/sbin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin

sqlplus sys/oracle@localhost:1521/cat as sysdba<< EOF

create user apptest identified by oracle;
grant all privileges to apptest;
grant gsmadmin_role to apptest;
grant select_catalog_role to apptest;
grant connect, resource to apptest;
grant dba to apptest;
grant execute on dbms_crypto to apptest;

CREATE TABLESPACE SET TSP_SET_1 using template (datafile size 8m autoextend on next 8M maxsize unlimited extent management local segment space management auto);


EOF
