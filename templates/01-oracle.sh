#!/bin/sh

export ORACLE_SID={{catalog_dbname}}
export ORACLE_HOME={{oracle_home}}
export PATH={{oracle_home}}/bin:/usr/sbin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin

sqlplus / as sysdba << EOF
spool /home/oracle/unlock_gsm_user.log;
alter user gsmcatuser identified by oracle account unlock;
create user mygds identified by oracle;
grant connect, create session, gsmadmin_role to mygds;
grant inherit privileges on user SYS to GSMADMIN_INTERNAL;
alter session enable shard ddl;
spool off;
EOF
