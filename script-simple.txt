----------------------------- SQL DBA - GSM
alter user gsmcatuser identified by oracle account unlock;
alter user GSMUSER identified by oracle account unlock;
create user mygds identified by oracle;
grant connect, create session, gsmadmin_role to mygds;
grant connect, create session, gsmadmin_role to gsmcatuser;
grant connect, create session, gsmadmin_role to GSMUSER;
grant SYSDG to GSMUSER;
grant SYSBACKUP to GSMUSER;
grant SYSDG to GSMADMIN_INTERNAL;
grant SYSBACKUP to GSMADMIN_INTERNAL;
grant read,write on directory DATA_PUMP_DIR to GSMADMIN_INTERNAL;
grant inherit privileges on user SYSDG to GSMUSER;
grant inherit privileges on user SYSDG to gsmcatuser;
grant inherit privileges on user SYSBACKUP to GSMUSER;
grant inherit privileges on user SYSBACKUP to gsmcatuser;
grant inherit privileges on user SYSBACKUP to GSMADMIN_INTERNAL;
grant inherit privileges on user SYS to GSMADMIN_INTERNAL;


----------------------------- GSM


create shardcatalog -sharding SYSTEM -database orcl-100:1521:shardcat -chunks 60 -user mygds/oracle -sdb shdb -region dc1 -agent_port 8888 -agent_password oracle

-force -repl OGG

add gsm -gsm sharddirector1 -listener 1522 -pwd oracle -catalog orcl-100:1521:shardcat -region dc1
start gsm -gsm sharddirector1
add credential -credential cre_reg1 -osaccount oracle -ospassword oracle
set gsm -gsm sharddirector1
connect mygds/oracle

export ORACLE_HOME=/u01/app/oracle/product/18.0.0/gsmhome_1
export PATH=$ORACLE_HOME/bin:$PATH






----------------------------- NODE
mkdir -p /u01/app/oracle/oradata
mkdir -p /u01/app/oracle/fast_recovery_area
schagent -start
echo oracle | schagent -registerdatabase 192.192.184.217 7777
echo oracle | schagent -registerdatabase 192.168.99.110 8888


----------------------------- GSM
add invitednode shard1
add invitednode shard2
add invitednode shard3


add shardgroup -shardgroup primary_sg1 -deploy_as primary -region dc1


add cdb -connect 192.168.99.101:1521:cdb1 -pwd oracle
add shard -cdb cdb1 -connect 192.168.99.101:1521/cdb1 -shardgroup primary_sg1 -pwd oracle
modify shard -shard cdb1 -connect orcl-101:1521/pdb1


create shard -shardgroup primary_sg1 -destination orcl-101 -credential cre_reg1 -sys_password oracle
create shard -shardgroup primary_sg1 -destination orcl-101 -credential cre_reg1 -sys_password oracle

 -dbtemplatefile /u01/app/oracle/product/12.2.0.1/db_1/assistants/dbca/templates/General_Purpose.dbc
create shard -shardgroup primary_sg1 -destination shard2 -credential cre_reg1 -sys_password oracle -dbtemplatefile /u01/app/oracle/product/12.2.0.1/db_1/assistants/dbca/templates/General_Purpose.dbc
create shard -shardgroup primary_sg1 -destination shard3 -credential cre_reg1 -sys_password oracle -dbtemplatefile /u01/app/oracle/product/12.2.0.1/db_1/assistants/dbca/templates/General_Purpose.dbc


create shard -shardgroup primary_sg1 -destination shard4 -credential cre_reg1 -sys_password oracle -dbtemplatefile /u01/app/oracle/product/12.2.0.1/db_1/assistants/dbca/templates/General_Purpose.dbc
create shard -shardgroup primary_sg1 -destination shard5 -credential cre_reg1 -sys_password oracle -dbtemplatefile /u01/app/oracle/product/12.2.0.1/db_1/assistants/dbca/templates/General_Purpose.dbc
create shard -shardgroup primary_sg1 -destination shard6 -credential cre_reg1 -sys_password oracle -dbtemplatefile /u01/app/oracle/product/12.2.0.1/db_1/assistants/dbca/templates/General_Purpose.dbc
deploy


create shard -shardgroup primary_sg1 -destination shard4 -credential cre_reg1 -sys_password oracle \
-dbparamfile /tmp/parameters.txt
-dbtemplatefile /u01/app/oracle/product/12.2.0.1/db_1/assistants/dbca/templates/General_Purpose.dbc


-gdbName shard1.example.com -initParams sort_area_size=200000

add service -service oltp_rw_srvc -role primary
start service -service oltp_rw_srvc
alter session set container=ORCLPDB;

----------------------------- SQL DBA GSM
alter session enable shard ddl;
create user apptest identified by oracle;
grant all privileges to apptest;
grant gsmadmin_role to apptest;
grant select_catalog_role to apptest;
grant connect, resource to apptest;
grant dba to apptest;
grant execute on dbms_crypto to apptest;


---------- modify db_files to support more tablespace, database restart is required
alter system set db_files = 2048 scope = spfile;

CREATE TABLESPACE SET TSP_SET_1 using template (datafile size 8m autoextend on next 8M maxsize unlimited extent management local segment space management auto);
CREATE TABLESPACE SET TSP_SET_1 using template (datafile size 2m autoextend on next 2M maxsize unlimited extent management local segment space management auto);

sqlplus apptest/oracle@'(description=(address=(protocol=tcp)(host=gsm1)(port=1522))(connect_data=(service_name=oltp_rw_srvc.shdb.oradbcloud)(region=dc1)(SHARDING_KEY=james.parker@x.bogus)))'
sqlplus apptest/oracle@192.168.99.100:1521/GDS\$CATALOG.oradbcloud


recover shard -shard sh1 -ignore_first
recover shard -shard sh1 -ignore_first
recover shard -shard sh1 -ignore_first
recover shard -shard sh1 -ignore_first
recover shard -shard sh1 -ignore_first
recover shard -shard sh1 -ignore_first
recover shard -shard sh1 -ignore_first
recover shard -shard sh1 -ignore_first
recover shard -shard sh1 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh2 -ignore_first
recover shard -shard sh3 -ignore_first

recover shard -shard sh1
recover shard -shard sh2
recover shard -shard sh3


---------- extends tmpfs
umount tmpfs
mount -t tmpfs shmfs -o size = 2048m  /dev/shm

-------------
ip link delete virbr0
ip link delete virbr0-nic

ansible-playbook -i inventory oracle.yaml -e "ansible_user=root ansible_ssh_pass=thinker ansible_sudo_pass=thinker" | tee deploy.log
ansible-playbook -i test test.yaml -e "ansible_user=root ansible_ssh_pass=thinker ansible_sudo_pass=thinker" | tee deploy.log



ssh-keyscan -H 192.192.184.200 >> ~/.ssh/known_hosts
ssh-keyscan -H 192.192.184.201 >> ~/.ssh/known_hosts
ssh-keyscan -H 192.192.184.202 >> ~/.ssh/known_hosts
ssh-keyscan -H 192.192.184.203 >> ~/.ssh/known_hosts

alter system set events 'immediate trace name GWM_TRACE level 7';
alter system set event='10798 trace name context forever, level 7' scope=spfile;

select count(*) from activity where target  in ('1','2','3','4','5','6','7','8','9');
select count(*) from activity where EVENT_TIME < current_date;